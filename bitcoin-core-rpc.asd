;;;; bitcoin-core-rpc.asd

(asdf:defsystem #:bitcoin-core-rpc
  :description "Describe bitcoin-core-rpc here"
  :author "Jim Hansson <jim.hansson@gmail.com>"
  :license  "LGPL"
  :version "0.0.1"
  :serial t
  :depends-on ("cl-async"
	       "carrier"
	       "blackbird"
	       "verbose")
  :pathname "src"
  :components ((:file "package")
	       (:file "bitcoin-core-rpc")
	       (:file "defrpc")
	       (:file "http-request")
	       (:file "blockchain")))
