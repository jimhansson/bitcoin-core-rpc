# bitcoin-core-rpc

### _Jim Hansson <jim.hansson@gmail.com>_

This project wraps the RPC interface of bitcoin core and allows you to
use it. It's built on cl-async and uses blackbird promise library.

## Configuration

It uses 3 main variables that need to be set, these are
+rpc-password+, +rpc-user+ and +rpc-host-url+. on load of the project
it will look in user home directory for a file called
.bitcoin-core-rpc.lisp and load that, this is after the all variables
have been declared so you can just (setq ...) them to the values you
need them to be.


## Versioning

We follow the versioning av bitcoin-core, but adds one more
number. This should make it simple to see what version should
match. Bitcoin core will probebly not be breaking any apis at regular
intervals, but still it is a good idea.

## example of usage

'''
(cl-async:with-event-loop ()
  (let ((block-hash "00000000839a8e6886ab5951d76f411475428afc90947ee320161bbf18eb6048"))
    (setq test (bitcoin-core-rpc::get-block block-hash :verbosity 2))
    (sleep 1)))
'''
## License

Specify license here

