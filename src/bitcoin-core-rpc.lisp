;;;; bitcoin-core-rpc.lisp

(in-package #:bitcoin-core-rpc)

(defparameter +rpc-password+ ""
  "Password for doing RPC request to bitcoin-core")

(defparameter +rpc-user+ ""
  "User used when doing RPC request to bitcoin-core")

(defparameter +rpc-host-url+ "http://localhost:8332/"
  "URL of bitcoin-core (remember the port)")

(defun load-user-config ()
  (handler-case
      (load
       (merge-pathnames
	".bitcoin-core-rpc.lisp"
	(user-homedir-pathname)))
    (error (e)
      (format *error-output* "Could not load user configuration: ~a~%" e))))

(load-user-config)

(defun parse-response (data)
  (let ((result (assoc :result data))
	(err    (assoc :error data))
	(id     (assoc :id data)))
    ;;(format t "~%parse-response data: ~a ~a~%" data (type-of (caar data)))
    (if (cdr err)
	(error 'rpc-error err))
    (unless (cdr result)
      (error "we have no result"))
    result))


