(in-package #:bitcoin-core-rpc)


(defrpc abandon-transaction (txid)
  (:documentation "The abandontransaction RPC marks an in-wallet
  transaction and all its in-wallet descendants as abandoned. This
  allows their inputs to be respent.

Mark in-wallet transaction <txid> as abandoned This will mark this
transaction and all its in-wallet descendants as abandoned which will
allow for their inputs to be respent. It can be used to replace
“stuck” or evicted transactions. 

It only works on transactions which are not included in a block and
are not currently in the mempool. 

It has no effect on transactions which are already abandoned."))

(defrpc abort-rescan ()
  (:documentation "The abortrescan RPC Stops current wallet rescan

Stops current wallet rescan triggered by an RPC call, e.g. by an
importprivkey call."))

(defrpc add-multisig-address (nrequired keys &key label address-type)
  (:documentation "The addmultisigaddress RPC adds a P2SH multisig
  address to the wallet. Add a nrequired-to-sign multisignature
  address to the wallet. Requires a new wallet backup.

Each key is a Bitcoin address or hex-encoded public key.

This functionality is only intended for use with non-watchonly addresses.

See importaddress for watchonly p2sh address support.

If ‘label’ is specified, assign address to that label."))

(defrpc add-node (node command)
  (:documentation "The addnode RPC attempts to add or remove a node
  from the addnode list. Or try a connection to a node once.

Nodes added using addnode (or -connect) are protected from DoS
disconnection and are not required to be full nodes/support SegWit as
other outbound peers are (though such peers will not be synced
from)."))

(defrpc analyze-psbt (psbt)
  (:documentation "The analyzepsbt RPC analyzes and provides
  information about the current status of a PSBT and its inputs."))

(defrpc backup-wallet (destination)
  (:documentation "The backupwallet RPC safely copies current wallet
  file to destination, which can be a directory or a path with
  filename."))

(defrpc bump-fee (txid &key options)
  (:documentation "The bumpfee RPC bumps the fee of an opt-in-RBF
  transaction T, replacing it with a new transaction B.

An opt-in RBF transaction with the given txid must be in the wallet.

The command will pay the additional fee by decreasing (or perhaps
removing) its change output.

If the change output is not big enough to cover the increased fee, the
command will currently fail instead of adding new inputs to
compensate. (A future implementation could improve this.) The command
will fail if the wallet or mempool contains a transaction that spends
one of T’s outputs.


By default, the new fee will be calculated automatically using
estimatesmartfee.

The user can specify a confirmation target for estimatesmartfee.

Alternatively, the user can specify totalFee, or use RPC settxfee to
set a higher fee rate.

At a minimum, the new fee rate must be high enough to pay an
additional new relay fee (incrementalfee returned by getnetworkinfo)
to enter the node’s mempool."))

(defrpc clear-banned ()
  (:documentation "The clearbanned RPC clear all banned IPs."))

(defrpc combine-psbt (txs)
  (:documentation "The combinepsbt RPC combine multiple partially
  signed Bitcoin transactions into one transaction.

Implements the Combiner role."))

(defrpc combine-raw-transaction (txs)
  (:documentation "The combinerawtransaction RPC combine multiple
  partially signed transactions into one transaction.

The combined transaction may be another partially signed transaction
or a fully signed transaction."))

(defrpc convert-to-psbt (hexstring &key permit-sig-data is-witness)
  (:documentation "The converttopsbt RPC converts a network serialized
  transaction to a PSBT.

This should be used only with createrawtransaction and
fundrawtransaction createpsbt and walletcreatefundedpsbt should be
used for new applications."))

(defrpc create-multisig (nrequired keys &key address-type)
  (:documentation "The createmultisig RPC creates a multi-signature
  address with n signature of m keys required.

It returns a json object with the address and redeemScript."))

(defrpc create-psbt (inputs outputs &key lock-time replaceable)
  (:documentation "The createpsbt RPC creates a transaction in the
  Partially Signed Transaction format.

Implements the Creator role."))

(defrpc create-raw-transaction (inputs outputs &key locktime replaceable)
  (:documentation "The createrawtransaction RPC create a transaction
  spending the given inputs and creating new outputs.

Outputs can be addresses or data.

Returns hex-encoded raw transaction.

Note that the transaction’s inputs are not signed, and it is not
stored in the wallet or transmitted to the network."))

(defrpc create-wallet (wallet-name &key disable-private-keys blank)
  (:documentation "The createwallet RPC creates and loads a new
  wallet."))

(defrpc decode-psbt (psbt)
  (:documentation "The decodepsbt RPC return a JSON object
  representing the serialized, base64-encoded partially signed Bitcoin
  transaction."))

(defrpc decode-raw-transaction (hexstring &key iswitness)
  (:documentation "The decoderawtransaction RPC return a JSON object
  representing the serialized, hex-encoded transaction."))

(defrpc decode-script (hexstring)
  (:documentation "The decodescript RPC decode a hex-encoded
  script."))

(defrpc derive-addresses (descriptor &key range)
  (:documentation "The deriveaddresses RPC derives one or more
  addresses corresponding to an output descriptor.

Examples of output descriptors are:

pkh(<pubkey>)                        P2PKH outputs for the given pubkey
wpkh(<pubkey>)                       Native segwit P2PKH outputs for the given pubkey
sh(multi(<n>,<pubkey>,<pubkey>,...)) P2SH-multisig outputs for the given threshold and pubkeys
raw(<hex script>)                    Outputs whose scriptPubKey equals the specified hex scripts

In the above, <pubkey> either refers to a fixed public key in
hexadecimal notation, or to an xpub/xprv optionally followed by one or
more path elements separated by “/”, where “h” represents a hardened
child key.

For more information on output descriptors, see the documentation in
the doc/descriptors.md file."))

(defrpc disconnect-node (&key address nodeid)
  (:documentation "The disconnectnode RPC immediately disconnects from
  the specified peer node.

Strictly one out of ‘address’ and ‘nodeid’ can be provided to identify
the node.

To disconnect by nodeid, either set ‘address’ to the empty string, or
call using the named ‘nodeid’ argument only."))

(defrpc dump-priv-key (address)
  (:documentation "The dumpprivkey RPC reveals the private key
  corresponding to ‘address’. Then the importprivkey can be used with
  this output"))

(defrpc dump-wallet (filename)
  (:documentation "The dumpwallet RPC dumps all wallet keys in a
  human-readable format to a server-side file.

This does not allow overwriting existing files.

Imported scripts are included in the dumpfile, but corresponding
BIP173 addresses, etc. may not be added automatically by importwallet.

Note that if your wallet contains keys which are not derived from your
HD seed (e.g. imported keys), these are not covered by only backing up
the seed itself, and must be backed up too (e.g. ensure you back up
the whole dumpfile)."))

(defrpc encrypt-wallet (passphrase)
  (:documentation "The encryptwallet RPC encrypts the wallet with
  ‘passphrase’. This is for first time encryption.

After this, any calls that interact with private keys such as sending
or signing will require the passphrase to be set prior the making
these calls.

Use the walletpassphrase call for this, and then walletlock call.

If the wallet is already encrypted, use the walletpassphrasechange
call."))

(defrpc estimate-smart-fee (conf-target &key estimate-mode)
  (:documentation "The estimatesmartfee RPC estimates the approximate
  fee per kilobyte needed for a transaction to begin confirmation
  within conf_target blocks if possible and return the number of
  blocks for which the estimate is valid.

Uses virtual transaction size as defined in BIP 141 (witness data is
discounted)."))

(defrpc finalize-psbt (psbt &key extract)
  (:documentation "The finalizepsbt RPC finalize the inputs of a PSBT.

If the transaction is fully signed, it will produce a network
serialized transaction which can be broadcast with
sendrawtransaction. Otherwise a PSBT will be created which has the
final_scriptSig and final_scriptWitness fields filled for inputs that
are complete.

Implements the Finalizer and Extractor roles."))

(defrpc fund-raw-transaction (hexstring &key options iswitness)
  (:documentation "The fundrawtransaction RPC add inputs to a
  transaction until it has enough in value to meet its out value.

This will not modify existing inputs, and will add at most one change
output to the outputs.

No existing outputs will be modified unless “subtractFeeFromOutputs”
is specified.

Note that inputs which were signed may need to be resigned after
completion since in/outputs have been added.

The inputs added will not be signed, use signrawtransactionwithkey or
signrawtransactionwithwallet for that.

Note that all existing inputs must have their previous output
transaction be in the wallet.

Note that all inputs selected must be of standard form and P2SH
scripts must be in the wallet using importaddress or
addmultisigaddress (to calculate fees).

You can see whether this is the case by checking the “solvable” field
in the listunspent output.

Only pay-to-pubkey, multisig, and P2SH versions thereof are currently
supported for watch-only"))

(defrpc generate (nblocks &key maxtries)
  (:documentation "The generate RPC mine up to nblocks blocks
  immediately (before the RPC call returns) to an address in the
  wallet."))

(defrpc generate-to-address (nblocks address &key maxtries)
  (:documentation "The generatetoaddress RPC mine blocks immediately
  to a specified address (before the RPC call returns)."))

(defrpc get-added-node-info (node)
  (:documentation "The getaddednodeinfo RPC returns information about
  the given added node, or all added nodes (note that onetry addnodes
  are not listed here)."))

(defrpc get-address-by-label (label)
  (:documentation "The getaddressesbylabel RPC returns the list of
  addresses assigned the specified label."))

(defrpc get-address-info (address)
  (:documentation "The getaddressinfo RPC return information about the
  given bitcoin address.

Some information requires the address to be in the wallet."))

(defrpc get-balance (&key dummy minconf include-watchonly)
  (:documentation "The getbalance RPC returns the total available balance.

The available balance is what the wallet considers currently
spendable, and is thus affected by options which limit spendability
such as -spendzeroconfchange."))

(defrpc get-best-block-hash ()
  (:documentation "The getbestblockhash RPC returns the hash of the
  best (tip) block in the longest blockchain."))

(defrpc get-block (blockhash &key verbosity)
  (:documentation "The getblock RPC gets a block with a particular
  header hash from the local block database either as a JSON object or
  as a serialized block. 

If verbosity is 0, returns a string that is serialized, hex-encoded
data for block ‘hash’. 

If verbosity is 1, returns an Object with information about block
‘hash’. 

If verbosity is 2, returns an Object with information about block
‘hash’ and information about each transaction." ))

(defrpc get-blockchain-info ()
  (:documentation "The getblockchaininfo RPC returns an object
  containing various state info regarding blockchain processing."))

(defrpc get-block-count ()
  (:documentation "The getblockcount RPC returns the number of blocks
  in the longest blockchain."))

(defrpc get-block-hash (height)
  (:documentation "The getblockhash RPC returns hash of block in
  best-block-chain at height provided."))

(defrpc get-block-header (blockhash &key verbose)
  (:documentation "The getblockheader RPC if verbose is false, returns
  a string that is serialized, hex-encoded data for blockheader
  ‘hash’.

If verbose is true, returns an Object with information about
blockheader ‘hash’."))

(defrpc get-block-stats (hash-or-height &key stats)
  (:documentation "The getblockstats RPC compute per block statistics
  for a given window. All amounts are in satoshis.

It won’t work for some heights with pruning.

It won’t work without -txindex for utxo_size_inc, *fee or *feerate
stats."))

(defrpc get-block-template (template-request)
  (:documentation "The getblocktemplate RPC if the request parameters
  include a ‘mode’ key, that is used to explicitly select between the
  default ‘template’ request or a ‘proposal’.

It returns data needed to construct a block to work on.

For full specification, see BIPs 22, 23, 9, and 145:"))

(defrpc get-chain-tips ()
  (:documentation "The getchaintips RPC return information about all
  known tips in the block tree, including the main chain as well as
  orphaned branches."))

(defrpc get-chain-tx-stats (&key nblocks blockhash)
  (:documentation "The getchaintxstats RPC compute statistics about
  the total number and rate of transactions in the chain."))

(defrpc get-connection-count ()
  (:documentation "The getconnectioncount RPC returns the number of
  connections to other nodes."))

(defrpc get-descriptor-info (descriptor)
  (:documentation "The getdescriptorinfo RPC analyses a descriptor."))

;;;;;;;;;; UNSORTED;;;;;;;;;;

(defrpc get-net-totals ()
  (:documentation "The getnettotals RPC returns information about
  network traffic, including bytes in, bytes out, and current time."))

(defrpc get-peer-info ()
  (:documentation "The getpeerinfo RPC returns data about each
  connected network node as a json array of objects."))

(defrpc get-rpc-info ()
  (:documentation "The getrpcinfo RPC returns details of the RPC
  server."))


(defrpc get-received-by-address (address &key minconf)
  (:documentation "The getreceivedbyaddress RPC returns the total
  amount received by the given address in transactions with at least
  minconf confirmations."))

(defrpc get-transaction (txid)
  (:documentation "The gettransaction RPC get detailed information
  about in-wallet transaction <txid>."))

(defrpc list-received-by-address (&key minconf
				       include-empty
				       include-watchonly
				       address-filter)
  (:documentation "The listreceivedbyaddress RPC list balance by receiving address."))


(defrpc utxo-update-psbt (psbt)
  (:documentation "The utxoupdatepsbt RPC updates a PSBT with witness
  UTXOs retrieved from the UTXO set or the mempool."))

(defrpc validate-address (address)
  (:documentation "The validateaddress RPC return information about
  the given bitcoin address."))

(defrpc wallet-process-psbt (psbt &key sign sighashtype)
  (:documentation "The walletprocesspsbt RPC update a PSBT with input
  information from our wallet and then sign inputs that we can sign
  for."))

(defmacro alias (to fn)
  "Lets us define better names for functions so we can make related
functions sort in lex order.

;(alias psbt/create create-psbt)
;(alias psbt/analyze analyze-psbt)
or we could put them in different sub-namespaces"  
  `(setf (fdefinition ',to) #',fn))

