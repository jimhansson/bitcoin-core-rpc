
(in-package #:bitcoin-core-rpc)



(defun to-json (data)
  (cl-json:encode-json-to-string data))

(defun from-json (json)
  (cl-json:decode-json-from-string json))


(defun my-http-request (method params)
  (let* ((request `(("jsonrpc" . "1.0")
		    ("id" . "asdf")
		    ("method" . ,method)
		    ("params" . ,params)))
	 (auth (concatenate 'string "Basic "
			    (base64:string-to-base64-string
			     (concatenate 'string +rpc-user+ ":" +rpc-password+))))
	 (headers `("Authorization" ,auth "content-type" "text/plain"))
	 (request-json (to-json request)))
    (format t "~%json: ~a~%" request-json)
    (blackbird:catcher
     (bb:multiple-promise-bind (body)
	 (carrier:request +rpc-host-url+
			  :headers headers
			  :method :post
			  :return-body t
			  :body request-json)
       (format t "~%reply: ~a~%" (babel:octets-to-string body))
       (from-json (babel:octets-to-string body)))
     (error (e)
	    (format t "Error: ~a~%" e)))))
