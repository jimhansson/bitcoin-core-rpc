(in-package #:bitcoin-core-rpc)

(defparameter +passed-var-suffix+ "-passed")

(defun deconstruct-lambda-list (lambda-list)
  (flet ((adapt-optional (opt)
	   (if (listp opt)
	       opt
	       `(,opt nil ,(make-symbol (concatenate 'string
  (string opt) +passed-var-suffix+))))))
    (let* ((key-pos (position '&KEY lambda-list))
	   (required (subseq lambda-list 0 key-pos))
	   (optionals
	    (if key-pos
		(map 'list #'adapt-optional (subseq lambda-list (1+ key-pos)))
		nil)))
      (values
       required
       optionals
       (concatenate 'list
		    required
		    (if key-pos '(&KEY) nil)
		    (if key-pos optionals))))))

(defmacro with-gensyms ((&rest names) &body body)
  `(let ,(loop for n in names collect `(,n (gensym ,(string n))))
     ,@body))

(defun fix-method-name (method-name)
  (remove #\- (string-downcase method-name)))


(defun replace-all (string part replacement &key (test #'char=))
  "Returns a new string in which all the occurences of the part 
is replaced with replacement."
  (with-output-to-string (out)
    (loop with part-length = (length part)
       for old-pos = 0 then (+ pos part-length)
       for pos = (search part string
			 :start2 old-pos
			 :test test)
       do (write-string string out
			:start old-pos
			:end (or pos (length string)))
       when pos do (write-string replacement out)
       while pos)))

(defun fix-parameter-name (name)
  (replace-all name "-" "_"))

(defmacro defrpc (fname llist options)
  (with-gensyms (alist response)
    (multiple-value-bind (llist-req llist-opt llist-cmplt)
	(deconstruct-lambda-list llist)
      (flet ((alist-pair (i)
	       `(setq ,alist
		      (cons
		       (cons
			,(fix-parameter-name (string-downcase (string i)))
			,i)
		       ,alist)))
	     (alist-pair-opt (v)
	       (flet ((v-name (v) (car v))
		      (v-defv (v) (cadr v))
		      (v-sup (v) (caddr v)))
		 `(when ,(v-sup v) (setq ,alist
					 (cons
					  (cons
					   ,(fix-parameter-name (string-downcase (string (v-name v))))
					   ,(v-name v))
					  ,alist))))))
	(let ((method-name (fix-method-name (string fname))))
	  `(progn
	     (defun ,fname ,llist-cmplt
	       (let ((,alist nil))
		 ;;,llist-opt
		 ;;,llist-cmplt
		 ,@(map 'list #'alist-pair llist-req)
		 ,@(map 'list #'alist-pair-opt llist-opt)
		 (bb:alet ((,response (my-http-request ,method-name
						       ,alist)))
		   ;; unwrap and only return the error or the result of
		   ;; the response
		   (when (cdr (assoc :error ,response))
		     (error (cdr (assoc :error ,response))))
		   (cdr (assoc :result ,response)))))
	     
	     (export ',fname)))))))

